package CRUD;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.conversions.Bson;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.orderBy;
import static java.util.Arrays.asList;

public class PeopleCRUD {

    public void friendsByPerson(MongoDatabase db) {
        MongoCollection<Document> collection = db.getCollection("people2");

        Bson friendsCol = project(
                Projections.fields(Projections.excludeId(),
                        Projections.include("name"),
                        Projections.include("friends")
                )
        );

        collection.aggregate(asList(friendsCol, unwind("$friends"), sort(ascending("name"))))
                .forEach( p -> {
                    System.out.println(p.get("name")+": \n\t- Friends : "+p.get("friends"));
                    System.out.println();
                });
    }

}
