package CRUD;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;
import org.bson.Document;
import org.bson.conversions.Bson;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Updates.addToSet;
import static java.util.Arrays.asList;

public class BookCRUD {

    public void moreThan5Books(MongoDatabase db) {
        MongoCollection<Document> collection = db.getCollection("books");

        Bson authorsCol = project(
                Projections.fields(Projections.excludeId(),
                        Projections.include("title"),
                        Projections.computed(
                                "numAuthors",
                                Projections.computed("$size", "$authors"))
                        )
                );

        collection.aggregate(asList(authorsCol, sort(descending("numAuthors"))))
                        .forEach( book -> {
                            if (book.getInteger("numAuthors") >= 5) {
                                System.out.println(book.get("title")+" -> "+book.get("numAuthors"));
                            }
                        });
    }

    public void mostCategories(MongoDatabase db) {
        MongoCollection<Document> collection = db.getCollection("books");

        Bson categoriesCol = project(
                Projections.fields(Projections.excludeId(),
                        Projections.include("title"),
                        Projections.include("categories"),
                        Projections.computed(
                                "numCategories",
                                Projections.computed("$size", "$categories")
                        )
                )
        );

        var ref = new Object() {
            int aux = 0;
        };
        collection.aggregate(asList(categoriesCol, sort(descending("numCategories"))))
                .forEach( book -> {
                    if (ref.aux < book.getInteger("numCategories"))
                        ref.aux = book.getInteger("numCategories");

                    if (book.getInteger("numCategories") >= ref.aux) {
                        System.out.println(book.get("title")+": "+book.get("categories")+" -> "+book.getInteger("numCategories"));
                    }
                });
    }

    public void authorsDesc(MongoDatabase db) {
        MongoCollection<Document> collection = db.getCollection("books");

        Bson authorsCol = project(
                Projections.fields(Projections.excludeId(),
                        Projections.include("title"),
                        Projections.include("authors"),
                        Projections.computed(
                                "numAuthors",
                                Projections.computed("$size", "$authors")
                        )
                )
        );

        collection.aggregate(asList(authorsCol, sort(descending("numAuthors"))))
                .forEach( book -> {
                    System.out.println(book.get("title")+": \n\t"+"- #Authors : "+book.getInteger("numAuthors")+"\n\t- "+book.get("authors")+"\n");
                });
    }

    public void isbnByStatus(MongoDatabase db) {
        MongoCollection<Document> collection = db.getCollection("books");

        Bson isbnCol = project(
                Projections.fields(Projections.excludeId(),
                        Projections.include("title"),
                        Projections.include("isbn")
                )
        );

        collection.aggregate(asList(isbnCol, group("$status"), addToSet("$isbn", "ISBN")))
                .forEach( book -> {
                    System.out.println(book.get("title")+":\n\t- "+book.get("isbn")+"\n");
                });
    }
}
