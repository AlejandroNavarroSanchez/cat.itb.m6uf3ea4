package CRUD;

import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.*;
import com.mongodb.client.result.InsertOneResult;
import models.CountryDTO;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.mongodb.client.model.Accumulators.max;
import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Sorts.ascending;
import static java.util.Arrays.asList;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class CountryCRUD {
    public void dropCountries(MongoDatabase db) {
        db.getCollection("countries").drop();
    }

    public void carregaCountries(CountryDTO[] countries, MongoDatabase db) {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClient.getDefaultCodecRegistry(),
                fromProviders(PojoCodecProvider.builder().automatic(true).build()));

        db = db.withCodecRegistry(pojoCodecRegistry);

        // Collection
        MongoCollection<CountryDTO> col = db.getCollection("countries", CountryDTO.class);

        for (CountryDTO c : countries) {
            System.out.println("Adding... " + c);
            col.insertOne(c);
        }

    }

    public CountryDTO[] getCountriesFromFile(String file) {
        StringBuilder stringFile = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String linea;
            while ((linea = br.readLine()) != null) {
                stringFile.append(linea);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

        Gson gson = new Gson();
        return gson.fromJson(stringFile.toString(), CountryDTO[].class);
    }

    public void crearIndex(MongoDatabase db, String key) {
        db.getCollection("countries").dropIndexes();
        db.getCollection("countries").createIndex(Indexes.ascending(key), new IndexOptions().unique(true));
    }

    public void llistaIndex(MongoDatabase db) {
        MongoCollection<Document> col = db.getCollection("countries");
        for ( Document index : col.listIndexes() ) {
            System.out.println(index.toJson());
        }
    }

    public void insertaCountry(MongoDatabase db, Document doc) {
        MongoCollection<Document> col = db.getCollection("countries");
        try {
            InsertOneResult result = col.insertOne(doc);
            System.out.println("Successfully inserted document: " + doc.toJson());

        } catch (MongoException e) {
            e.printStackTrace();
        }
    }

    public void matchAndCount(MongoDatabase db) {
        Document englishSpeakingCountries = db.getCollection("countries")
                .aggregate(
                        asList(
                                match(Filters.eq("languages.name", "English")),
                                count()))
                .first();

//        assertEquals(91, englishSpeakingCountries.get("count"));
        assert englishSpeakingCountries != null;
        System.out.println(englishSpeakingCountries.get("count"));
    }

    public void groupAndSort(MongoDatabase db) {
        Document maxCountriedRegion = db.getCollection("countries").aggregate(asList(
                group("$region", sum("tally", 1)),
                sort(Sorts.descending("tally")))).first();

//        assertTrue(maxCountriedRegion.containsValue("Africa"));
        assert maxCountriedRegion != null;
        System.out.println(maxCountriedRegion.containsValue("Africa"));
    }

    public void sortLimitAndOut(MongoDatabase db) {
        db.getCollection("countries").aggregate(asList(
                sort(Sorts.descending("area")),
                limit(7),
                out("largest_seven"))).toCollection();

        MongoCollection<Document> largestSeven = db.getCollection("largest_seven");

//        assertEquals(7, largestSeven.countDocuments());
        System.out.println(largestSeven.countDocuments());

        Document usa = largestSeven.find(Filters.eq("alpha3Code", "USA")).first();

//        assertNotNull(usa);
        assert usa != null;
        System.out.println(usa.toJson());
    }

    public void projectGroupAndMatch(MongoDatabase db) {
        Bson borderingCountriesCollection = project(Projections.fields(Projections.excludeId(),
                Projections.include("name"), Projections.computed("borderingCountries",
                        Projections.computed("$size", "$borders"))));

        int maxValue = Objects.requireNonNull(db.getCollection("countries")
                        .aggregate(
                                asList(
                                        borderingCountriesCollection,
                                        group(null, max("max", "$borderingCountries"))))
                        .first())
                .getInteger("max");

//        assertEquals(15, maxValue);
        System.out.println(maxValue);

        Document maxNeighboredCountry = db.getCollection("countries").aggregate(asList(borderingCountriesCollection,
                match(Filters.eq("borderingCountries", maxValue)))).first();

//        assertTrue(maxNeighboredCountry.containsValue("China"));
        assert maxNeighboredCountry != null;
        System.out.println(maxNeighboredCountry.containsValue("China"));
    }

    public void countriesBySubregion(MongoDatabase db) {
        AggregateIterable<Document> doc = db.getCollection("countries")
                .aggregate(
                        asList(
                                group("$subregion", sum("CountriesTotal", 1)),
                                sort(Sorts.descending("_id"))));

//        assertTrue(doc.containsValue("Africa"));
        for ( Document d : doc ) {
            System.out.println(d.toJson());
        }
    }

    public void maxLaungagesByCountry(MongoDatabase db) {
        Bson countriesCollection = project(
                Projections.fields(
                        Projections.excludeId(),
                        Projections.include("name"),
                        Projections.computed("totalLanguages",
                        Projections.computed("$size", "$languages"))));

        int maxValue = Objects.requireNonNull(db.getCollection("countries")
                .aggregate(
                        asList(
                                countriesCollection,
                                group(null, max("max", "$totalLanguages"))))
                .first())
                .getInteger("max");

        Document maxLanguagedCountry = db.getCollection("countries")
                .aggregate(
                        Arrays.asList(
                                countriesCollection,
                                match(Filters.eq("totalLanguages", maxValue))))
                .first();

        assert maxLanguagedCountry != null;
        System.out.println(maxLanguagedCountry.toJson());
    }
}
