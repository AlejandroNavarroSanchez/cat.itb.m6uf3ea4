import CRUD.BookCRUD;
import CRUD.CountryCRUD;
import CRUD.PeopleCRUD;
import com.mongodb.client.MongoDatabase;
import connection.ConnectionMongoCluster;
import models.CountryDTO;
import org.bson.Document;

public class Main {
    private static final CountryCRUD countryCRUD = new CountryCRUD();
    private static final BookCRUD bookCRUD = new BookCRUD();
    private static final PeopleCRUD peopleCRUD = new PeopleCRUD();

    public static void main(String[] args) {
        // Database
        MongoDatabase db = ConnectionMongoCluster.getDatabase("ITB");

        // EXERCISES
//        ex1(db, "src/main/resources/countries.json");

//        ex2_1(db, "name");
//        ex2_2(db);
//        ex2_3(db, new Document()
//                .append("_id", new ObjectId())
//                .append("name", "Spain"));

//        ex3_aa(db);
//        ex3_ab(db);
//        ex3_ac(db);
//        ex3_ad(db);
//        ex3_b(db);
//        ex3_c(db);
//        ex3_d(db);
//        ex3_e(db);
//        ex3_f(db);
//        ex3_g(db);
        ex3_h(db);

        // Close connection
        ConnectionMongoCluster.closeConnection();
    }

    private static void ex3_h(MongoDatabase db) {
        peopleCRUD.friendsByPerson(db);
    }

    private static void ex3_g(MongoDatabase db) {
        bookCRUD.isbnByStatus(db);
    }

    private static void ex3_f(MongoDatabase db) {
        bookCRUD.authorsDesc(db);
    }

    private static void ex3_e(MongoDatabase db) {
        bookCRUD.mostCategories(db);
    }

    private static void ex3_d(MongoDatabase db) {
        bookCRUD.moreThan5Books(db);
    }

    private static void ex3_c(MongoDatabase db) {
        countryCRUD.maxLaungagesByCountry(db);
    }

    private static void ex3_b(MongoDatabase db) {
        countryCRUD.countriesBySubregion(db);
    }

    private static void ex3_ad(MongoDatabase db) {
        countryCRUD.projectGroupAndMatch(db);
    }

    private static void ex3_ac(MongoDatabase db) {
        countryCRUD.sortLimitAndOut(db);
    }

    private static void ex3_ab(MongoDatabase db) {
        countryCRUD.groupAndSort(db);
    }

    private static void ex3_aa(MongoDatabase db) {
        countryCRUD.matchAndCount(db);
    }

    private static void ex2_3(MongoDatabase db, Document doc) {
        countryCRUD.insertaCountry(db, doc);
    }

    private static void ex2_2(MongoDatabase db) {
        countryCRUD.llistaIndex(db);
    }

    private static void ex2_1(MongoDatabase db, String key) {
        countryCRUD.crearIndex(db, key);
    }

    private static void ex1(MongoDatabase db, String file) {
        // Drop collection
        countryCRUD.dropCountries(db);
        // Get file array
        CountryDTO[] countries = countryCRUD.getCountriesFromFile(file);
        // Insert pojo array into collection
        countryCRUD.carregaCountries(countries, db);

    }
}
